from .Item import Item

class Track(Item):
    def __init__(self, details: dict):
        super().__init__(details)
        self.name = details["name"]

    def __str__(self):
        return "{:<25} {}".format(self.id, self.name)

    @classmethod
    def get_by_id(cls, id:str):
        """ TODO: ... """
        return Track(cls._SP.track(id))

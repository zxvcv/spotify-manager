import spotipy

from typing import List
from spotipy.oauth2 import SpotifyOAuth

from .Item import Item
from .User import User
from .Track import Track
from .Playlist import Playlist
from spotify.util import get_all


class Spotify():
    """ """

    _CLIENT_ID = "197930beda0f461abee68e06933a7df4"
    _CLIENT_SECRET = ""
    _REDIRECT_URI = "http://localhost:8080"

    def __init__(self):
        oauth = SpotifyOAuth(client_id=self._CLIENT_ID, client_secret=self._CLIENT_SECRET,
            redirect_uri=self._REDIRECT_URI, scope="playlist-modify-public", show_dialog=True)
        self.sp = spotipy.Spotify(client_credentials_manager=oauth)
        Item._SP = self.sp

    #deprecated (User.get_current)
    def get_current_user(self) -> User:
        """ TODO: ... """
        return User(self.sp.current_user())

    #deprecated (Playlist.create_for_user)
    def create_user_playlist(self, name: str) -> Playlist:
        """ TODO: ... """
        return Playlist(self.sp.user_playlist_create(self.get_current_user().id, name))

    #deprecated (Playlist.get_by_id)
    @classmethod
    def get_playlist(cls, playlist_id: str) -> Playlist:
        """ TODO: ... """
        return Playlist(cls._SP.playlist(playlist_id))

    #deprecated (Track.get_by_id)
    @classmethod
    def get_track(cls, track_id: str) -> Track:
        """ TODO: ... """
        return Track(self._SP.track(track_id))

    #deprecated (Playlist.get_by_user)
    def get_user_playlists(self):
        """ Generator - TODO: ... """
        for pl in get_all(self.sp.current_user_playlists):
            yield Playlist(pl)

    def get_tracks_in_playlists(self, playlists: List[Playlist] = []):
        """ Generator - Returns tracks with no repetitions. """
        tracks_id = []
        for pl in playlists:
            for tr in pl.get_tracks(limit=pl.tracks_total):
                if not (tr.id in tracks_id):
                    tracks_id.append(tr.id)
                    yield tr

    @classmethod
    def get_playlists_with_track(cls, track: Track, playlists: List[Playlist] = []):
        """ Generator - Return all of those playlists that contains passed track. """
        for pl in playlists:
            if pl.contains_track(track):
                yield pl

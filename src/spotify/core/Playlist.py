from typing import List
from itertools import islice

from .Item import Item
from .User import User
from .Track import Track
from spotify.util import get_all, create_chunks, get_items

class Playlist(Item):
    def __init__(self, details: dict):
        super().__init__(details)
        self.name = details["name"]
        self.description = details["description"]
        self.owner = User(details["owner"])
        self.tracks_total = details["tracks"]["total"]

    def __str__(self):
        return "{:<25} {:<20} {}".format(self.id, self.owner.id, self.name)

    @classmethod
    def get_by_user(cls, user:User = None) -> []:
        """ TODO: ... """
        if not user :
            user = User.get_current()

        for pl in get_all(cls._SP.user_playlists, user=user):
            yield Playlist(pl)

    @classmethod
    def get_by_id(cls, id:str):
        """ TODO: ... """
        return Playlist(cls._SP.playlist(id))

    @classmethod
    def create_for_user(cls, name:str, user:User = None):
        """ TODO: ... """
        if not user :
            user = User.get_current()

        return Playlist(cls._SP.user_playlist_create(user, name))

    def get_tracks(self, shuffled=False, repetition=False, cyclic=False, limit:int=None):
        """ Returns generator that allow to get tracks in multiple ways

            Arguments:
                shuffled    - if True tracks will be returned in random order
                repetition  - repetition of tracks allowed
                cyclic      - all values needs to be returned before they will start repeating themselves
                limit       - set limit of data to get, if None, data will be returned forever (with repetitions)
        """
        def getter(idx):
            return Track(self._SP.playlist_tracks(limit=1, offset=idx, playlist_id=self.id)["items"][0]["track"])

        return get_items(getter, self.tracks_total, shuffled=shuffled, repetition=repetition, cyclic=cyclic, limit=limit)

    def add_tracks(self, tracks: List[Track]) -> None:
        """ Deprecated TODO: ... """
        for track_chunk in create_chunks(tracks, 50):
            self._SP.user_playlist_add_tracks(self.owner.id, self.id,
                [t.id for t in track_chunk])

    def contains_track(self, track: Track) -> bool:
        """ TODO: ... """
        return any(track == tr for tr in self.get_tracks(limit=self.tracks_total))
